const DROP_ITEM = [...document.querySelectorAll('.header__dropdown-link_first')];
if (DROP_ITEM) {
    openDropdown();
}

function openDropdown() {
    const DROP_DOWN = [...document.querySelectorAll('.header__dropdown_first')];
    const DROP_DOWN_INNER = [...document.querySelectorAll('.header__dropdown_second')];
    const DROP_ITEM_INNER = [...document.querySelectorAll('.header__dropdown-link_second')];

    mainDrop(DROP_DOWN, DROP_ITEM);
    mainDrop(DROP_DOWN_INNER, DROP_ITEM_INNER)
}

function mainDrop(link, menu) {
    for (let i = 0; i < link.length; i++) {
        menu[i].addEventListener('mouseover', () => {
            link[i].classList.add('disblk')

        });
        menu[i].addEventListener('mouseleave', () => {
            link[i].classList.remove('disblk')

        })

    }
    link.forEach(item => {
        item.addEventListener('mouseleave', () => {
            item.classList.remove('disblk')
        });
        item.addEventListener('mouseenter', () => {
            item.classList.add('disblk')
        })
    })
}