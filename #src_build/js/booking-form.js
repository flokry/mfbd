const CALENDAR_OPEN = document.querySelector('.booking__open-calendar');
const CALENDAR = document.querySelector('.booking__calendar-wrapper');
const BOOKING_FORM = document.forms.school;

const FORM = {};

function send(formType, formObj) {
    localStorage.setItem(formType, formObj);
}
const newDate = new Date();

function radioCheck() {
    const TIMEINT = BOOKING_FORM.querySelector('.booking__time-interval-evening');
    if (event.target.value == 'night') {
        TIMEINT.innerHTML = `<label class="booking__radio-interval">
        <input type="radio" name="time-interval" value="17-19" checked="checked"/>
        <span>17:00-19:00</span></label>
        <label class="booking__radio-interval">
        <input type="radio" name="time-interval" value="17-19" />
        <span>19:00-21:00</span></label>
        <label class="booking__radio-interval">
        <input type="radio" name="time-interval" value="17-19"/>
        <span>21:00-23:00</span></label>`
    } else if (event.target.value == 'day') {
        TIMEINT.innerHTML = `<label class="booking__radio-interval">
        <input type="radio" name="time-interval" value="10-11" checked="checked"/>
        <span>10:00-11:00</span></label>
        <label class="booking__radio-interval">
        <input type="radio" name="time-interval" value="11-12"/><span>11:00-12:00</span></label>
        <label class="booking__radio-interval">
        <input type="radio" name="time-interval" value="12-13" />
        <span>12:00-13:00</span></label>`
    }
}


function formAdd() {
    FORM.dayTime = BOOKING_FORM.elements.time.value;
    FORM.timeInterval = BOOKING_FORM.elements['time-interval'].value;
    FORM.userName = BOOKING_FORM.elements['user_name'].value;
    FORM.userPhone = BOOKING_FORM.elements['user_phone'].value;
    FORM.rezervDate = newDate.getDate();
    FORM.rezervMonth = newDate.getMonth();
    FORM.rezervYear = newDate.getFullYear();

}

function checkInput() {
    if (/^([0-9]{3}|[0]{1})[0-9]{9}$/g.test(FORM.userPhone) && /^[а-яА-Яa-zA-Z]+$/.test(FORM.userName)) {
        BOOKING_FORM.elements.submit.classList.remove('disabled');
        BOOKING_FORM.elements.submit.removeAttribute('disabled');
    } else {
        BOOKING_FORM.elements.submit.classList.add('disabled');
        BOOKING_FORM.elements.submit.setAttribute('disabled', 'disabled');
    }
}

function confirmPopup() {
    let checkPopup = document.querySelector('.confirm');
    if (!checkPopup) {
        let popup = document.createElement('div');
        popup.classList.add('confirm');
        popup.innerHTML = '<div class="confirm__info"><p class="confirm__text">Дякуємо за бронювання екскурсії! Ми зв’яжемося з Вами найближчим часом.</p></div><a href="index.html" class="confirm__btn yellow-btn">На Головну</a>'
        document.body.prepend(popup);
    }
}

if (BOOKING_FORM) {
    const labelArr = [...BOOKING_FORM.querySelectorAll('.booking__input')];
    BOOKING_FORM.addEventListener('click', event => {
        radioCheck()
    })
    BOOKING_FORM.elements.submit.addEventListener('click', () => {
        event.preventDefault();

        send(BOOKING_FORM.name, JSON.stringify(FORM));
        confirmPopup();

    });
    BOOKING_FORM.elements['user_name'].addEventListener('input', () => {
        setInterval(() => {
            if (/^[а-яА-Яa-zA-Z]+$/.test(FORM.userName)) {
                labelArr[0].classList.add('checked');
            } else {
                labelArr[0].classList.remove('checked');
            }
        }, 500);
        formAdd();
        checkInput();
    });
    BOOKING_FORM.elements['user_phone'].addEventListener('input', () => {
        setInterval(() => {
            if (/^((380)|[0]{1})[0-9]{9}$/g.test(FORM.userPhone)) {
                labelArr[1].classList.add('checked');
            } else {
                labelArr[1].classList.remove('checked');
            }
        }, 500);
        formAdd();
        checkInput();
    })
}

if (CALENDAR_OPEN && CALENDAR) {
    CALENDAR_OPEN.addEventListener('click', event => {
        CALENDAR.classList.toggle('open');
        CALENDAR.classList.toggle('close');
    })


    CALENDAR.addEventListener('click', event => {
        const EXCURS_CALENDAR = document.querySelector('.booking__calendar-wrapper .calendar');
        if (EXCURS_CALENDAR && CALENDAR.classList[1] === 'open' && event.target.classList[0] === 'box') {
            let activeDay = EXCURS_CALENDAR.querySelector('.active');
            if (activeDay) {

                activeDay.classList.remove('active');
            }

            event.target.classList.add('active');
            FORM.rezervDate = activeDay.innerHTML;
        }
        FORM.rezervMonth = parseFloat(EXCURS_CALENDAR.rows[0].cells[1].dataset.month);
        FORM.rezervYear = parseFloat(EXCURS_CALENDAR.rows[0].cells[1].dataset.year);
    })
}