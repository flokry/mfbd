const BURGER_MENU = document.querySelector('.burger-menu');
const BURGER_LINE1 = document.querySelector('.burger-menu__line1');
const BURGER_LINE2 = document.querySelector('.burger-menu__line2');
const BURGER_LINE3 = document.querySelector('.burger-menu__line3');


BURGER_MENU.addEventListener('click', ()=>{
    BURGER_LINE1.classList.toggle('burger-menu__line1_open');
    BURGER_LINE2.classList.toggle('burger-menu__line2_open');
    BURGER_LINE3.classList.toggle('burger-menu__line3_open');
});