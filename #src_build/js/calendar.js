const CALENDARS_ARR = [...document.querySelectorAll('.calendar')];
CALENDARS_ARR.forEach(item => {
    Calendar("." + item.getAttribute('class'), new Date().getFullYear(), new Date().getMonth());

    // переключатель минус месяц
    item.rows[0].cells[0].onclick = function() {
        Calendar(".calendar", item.rows[0].cells[1].dataset.year, parseFloat(item.rows[0].cells[1].dataset.month) - 1);
    };
    // переключатель плюс месяц
    item.rows[0].cells[2].onclick = function() {
        Calendar(".calendar", item.rows[0].cells[1].dataset.year, parseFloat(item.rows[0].cells[1].dataset.month) + 1);
    };


    function Calendar(clas, year, month) {
        var lastDayMonth = new Date(year, month + 1, 0).getDate(), //Переменная последнего дня месяца
            date = new Date(year, month, lastDayMonth), //Полная дата текущего месяца
            lastWeekDayOfMonth = new Date(date.getFullYear(), date.getMonth(), lastDayMonth).getDay(), //Последний день недели для последнего дня месяца
            firstWeekDayOfMonth = new Date(date.getFullYear(), date.getMonth(), 1).getDay(), //Первый день недели для первого дня месяца
            calendar = '<tr>', //Cтрока
            month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
        if (firstWeekDayOfMonth != 0) {
            for (var i = 1; i < firstWeekDayOfMonth; i++) calendar += '<td>'; //Отслеживаем первый день месяца в неделе и забиваем пустыми ячейками до него
        } else {
            for (var i = 0; i < 6; i++) calendar += '<td>';
        }
        for (var i = 1; i <= lastDayMonth; i++) {
            if (i == new Date().getDate() && date.getFullYear() == new Date().getFullYear() && date.getMonth() == new Date().getMonth()) {
                calendar += '<td class="active box">' + i;
            } else if (i > new Date().getDate() && date.getMonth() == new Date().getMonth() || date.getMonth() > new Date().getMonth() || date.getFullYear() > new Date().getFullYear()) {
                calendar += '<td data-set="link" class="box">' + i;
            } else {
                calendar += '<td data-set="link">' + i;
            }
            if (new Date(date.getFullYear(), date.getMonth(), i).getDay() == 0) {
                calendar += '<tr>';
            }
        }

        for (var i = lastWeekDayOfMonth; i < 7; i++) calendar += '<td>&nbsp;';
        item.tBodies[0].innerHTML = calendar;
        item.tHead.rows[0].cells[1].innerHTML = month[date.getMonth()] + ' ' + date.getFullYear();
        item.tHead.rows[0].cells[1].dataset.month = date.getMonth();
        item.tHead.rows[0].cells[1].dataset.year = date.getFullYear();

        if (item.tBodies[0].rows.length < 6) { // чтобы при перелистывании месяцев не "подпрыгивала" вся страница, добавляется ряд пустых клеток. Итог: всегда 6 строк для цифр
            item.tBodies[0].innerHTML += '<tr><td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;<td>&nbsp;';
        }

    }

});