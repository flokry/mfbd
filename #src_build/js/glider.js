import Glide from '@glidejs/glide';


let blogSlider = document.querySelector('.blog__slider');
let gallerySlider = document.querySelector('.gallery__slider');
let reviewsSlider = document.querySelector('.reviews__slider');

if (reviewsSlider) {
    new Glide(reviewsSlider, {
        perView: 3,
        type: 'carousel',
        gap: 30,
        breakpoints: {
            1024: {
                perView: 2,
                perTouch: 1,
                dragThreshold: 10
            },
            760: {
                perView: 1,
                perTouch: 1,
                dragThreshold: 10
            },
        }
    }).mount();
}

if (gallerySlider) {
    new Glide(gallerySlider, {
        perView: 3,
        type: 'carousel',
        gap: 30,
        breakpoints: {
            1024: {
                perView: 2,
                perTouch: 1,
                dragThreshold: 10
            },
            600: {
                perView: 1,
                perTouch: 1,
                dragThreshold: 10
            },
        }
    }).mount();
}

if (blogSlider) {
    new Glide(blogSlider, {
        perView: 4,
        type: 'carousel',
        gap: 30,
        breakpoints: {
            1200: {
                perView: 3,
                perTouch: 1,
                dragThreshold: 10
            },

            760: {
                perView: 1,
                perTouch: 1,
                dragThreshold: 10
            },
        }

    }).mount();
}